# Attacking a neural network with adversarial attacks

## Assignment
The aim of my semestral work was to create a dataset with adversarial attacks according to one of the existing methods. This dataset is then used in another semestral work ([repository](https://gitlab.fit.cvut.cz/pudisond/mvi-sp)), which tries to protect a neural network from these attacks.

## Approach
My semestral work was created in four parts:

### Dataset selection
First we had to select the dataset and create a baseline classifier. We wanted to choose a dataset that would contain realistic photos so that it would not be too easy to detect adversarial images created by adding noise (eg. on the MNIST dataset it would be easy to notice the noise on a pure black background). We found [this dataset](https://www.kaggle.com/andrewmvd/animal-faces) from kaggle suitable. The dataset contains the faces of cats, dogs and wild animals. After downloading, we renamed *val* folder to *test*.

### Baseline model
Classifier training and evaluation is in notebooks [classifier_training](./classifier_training.ipynb) and [classifier_evaluation](./classifier_evaluation.ipynb). Its classification accuracy on the test data was almost 95%. The baseline model is stored in the file [best_model](./ best_model).

### Creating adversarial attacks
After researching various methods, I decided to generate adversarial attacks using FGSM (fast gradient sign method). Code for this section is in notebook [attack_FGSM](./attack_FGSM.ipynb). The result is a dataset that can be downloaded by clicking at [this link](https://drive.google.com/file/d/17EKQ5fZxutu8Op7YJ8U3yc2T1ueN325i/view?usp=sharing).

### Report
The last part was conduct to create a [report](./report.pdf) with a research of existing methods and a presentation of my results.

### Medium article
I've also made an article about adversarial attack that can be viewed [here](https://medium.com/@betkagogolakova/adversarial-attacks-1-the-theory-behind-a4de66ad6a05).

